﻿#include <iostream>
using namespace std;

class Animals
{
public:
    virtual void Voice()
    {
        cout << "null" << endl;
    }
};

class Dog : public Animals
{
public:
    void Voice() override
    {
        cout << "Woof!" << endl;
    }
};

class Cat : public Animals
{
public:
    void Voice() override
    {
        cout << "Miau" << endl;
    }
};

class Pigeon : public Animals
{
public:
    void Voice() override
    {
        cout << "Kurli-kurli" << endl;
    }
};

int main()
{
    Animals* pDog = new Dog;
    Animals* pCat = new Cat;
    Animals* pPigeon = new Pigeon;

    Animals* arr[3]{ pDog, pCat, pPigeon };

    for (int i = 0; i < 3; i++)
    {
        arr[i]->Voice();
    }

    delete pDog, pCat, pPigeon;
    pDog = pCat = pPigeon = nullptr;

}